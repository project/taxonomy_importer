<?php

namespace Drupal\taxonomy_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Taxonomy importer form class.
 */
class TaxonomyImporterForm extends FormBase {

  /**
   * The getFormId function.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'taxonomy_importer_form';
  }

  /**
   * BuildForm function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['container'] = [
      '#type' => 'fieldset',
    ];

    $form['container']['vocabulary_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Taxonomy Vocabulary'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the name of the taxonomy vocabulary that will be created.'),
    ];

    $form['container']['upload_csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('CSV File'),
      '#upload_location' => 'public://taxonomy_importer_csv',
      '#upload_validators' => [
        'FileExtension' => ['extensions' => 'csv CSV'],
      ],
      '#description' => $this->t('Attach your .csv file to create the taxonomies'),
    ];

    $form['container']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uploaded_csv = $form_state->getValue('upload_csv', 0);

    if (!is_null($uploaded_csv) && isset($uploaded_csv[0])) {
      $file = File::load($uploaded_csv[0]);
      $file->setPermanent();
      $file->save();
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$this->vocabularyFieldIsValid($form_state)) {
      $error = $this->t("The vocabulary '@name' alread exists and cannot be replaced.", [
        '@name' => $this->getVocabularyName($form_state),
      ]);
      $this->messenger()->addError($error);
      return;
    }

    $taxonomiesHelper = \Drupal::service('taxonomy_importer.taxonomies_helper');
    $vid = $this->getVocabularyId($form_state);
    $taxonomiesHelper->createVocabulary($vid, $this->getVocabularyName($form_state));

    $batch = $this->setBatch();

    $file_id = reset($form_state->getValue('upload_csv'));
    $file = File::load($file_id);
    if ($handle = fopen($file->getFileUri(), 'r')) {
      while ($line = fgetcsv($handle)) {
        if (!is_null($line[0])) {
          $batch['operations'][] = [
            '\Drupal\taxonomy_importer\Batch\BatchImporter::importLines',
            [array_map('base64_encode', $line), $vid],
          ];
        }
        else {
          $error_message = $this->t('Cannot import the file. Check it and try again.');
          $this->messenger()->addError($error_message);
          $taxonomiesHelper->removeVocabulary($vid);
          return;
        }
      }
      fclose($handle);
    }

    batch_set($batch);

  }

  /**
   * Function to get the vocabulary name from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   Returns the value from vocabulary_name field.
   */
  private function getVocabularyName(FormStateInterface $form_state) {
    return $form_state->getValue('vocabulary_name');
  }

  /**
   * Function to get the vocabulary id based on vocabulary_name form field.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   Returns the vocabulary id created.
   */
  private function getVocabularyId(FormStateInterface $form_state) {
    $vid = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '_',
      $this->getVocabularyName($form_state)));

    return $vid;
  }

  /**
   * Function check if value of vocabulary name is valid based on the vocabulary_id that will be created.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   Returns if vocabulary name field is valid.
   */
  private function vocabularyFieldIsValid(FormStateInterface $form_state) {
    $taxonomiesHelper = \Drupal::service('taxonomy_importer.taxonomies_helper');
    $vocabulary_id = $this->getVocabularyId($form_state);
    if ($taxonomiesHelper->checkIfVocabularyExists($vocabulary_id)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Function to build the initial structure of batch.
   *
   * @return array
   *   Returns the initial structure of batch.
   */
  private function setBatch() {
    return [
      'title' => $this->t('Importing csv file'),
      'init_mesage' => $this->t('The import is processing...'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'operations' => [],
      'finished' => '\Drupal\taxonomy_importer\Batch\BatchImporter::finishBatch',
    ];
  }

}
