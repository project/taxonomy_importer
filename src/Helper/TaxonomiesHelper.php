<?php

namespace Drupal\taxonomy_importer\Helper;

use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The taxonomy helper class.
 */
class TaxonomiesHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * The create function.
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
      );
  }

  /**
   * Function to check if taxonomy vocabulary already exists.
   *
   * @param string $vocabulary_id
   *   The id of the vocabulary.
   *
   * @return mixed
   *   Return an EntityInterface object if exists or null if doesn't.
   */
  public function checkIfVocabularyExists($vocabulary_id) {
    return $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vocabulary_id);
  }

  /**
   * Function to create a taxonomy vocabulary.
   *
   * @param string $vocabulary_id
   *   The id of vocabulary.
   * @param string $vocabulary_name
   *   The name of the vocabulary.
   *
   * @return void
   *   This function doesn't return any value.
   */
  public function createVocabulary($vocabulary_id, $vocabulary_name) {
    $this->entityTypeManager->getStorage('taxonomy_vocabulary')->create([
      'vid' => $vocabulary_id,
      'description' => '',
      'name' => $vocabulary_name,
    ])->save();
  }

  /**
   * Function to remove/delete a taxonomy vocabulary.
   *
   * @param string $vocabulary_id
   *   The vocabulary id of the vocabulary that will be removed.
   *
   * @return void
   *   This function doesn't return any value.
   */
  public function removeVocabulary($vocabulary_id) {
    $vocabulary = $this->checkIfVocabularyExists($vocabulary_id);
    if ($vocabulary) {
      $vocabulary->delete();
    }
  }

  /**
   * Function to create a taxonomy term.
   *
   * @param array $properties
   *   The properties of the term that will be created.
   * @param mixed $parent_id
   *   The parent id value, if the term has a parent.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The term created.
   */
  public function createTerm($properties, $parent_id = NULL) {
    if (!is_null($parent_id)) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->create($properties);
      $term->set('parent', ['target_id' => $parent_id]);
      $term->save();

      return $term;
    }
    return $this->entityTypeManager->getStorage('taxonomy_term')->create($properties)->save();
  }

  /**
   * Function to get the term.
   *
   * @param array $properties
   *   The properties of term.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return the EntityInterface
   */
  public function getTerm($properties) {
    return reset($this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($properties));
  }

  /**
   * Function to check the taxonomy vocabulary tree.
   *
   * @param string $vocabulary_id
   *   The id of taxonomy vocabulary.
   * @param string $term_name
   *   The name of term we want to check if is on taxonomy vocabulary tree.
   *
   * @return bool
   *   Return TRUE if term is on tree and FALSE if it is not.
   */
  public function checkTree($vocabulary_id, $term_name) {
    $tree = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary_id, 0, NULL, TRUE);

    $parentTree = [];
    if ($tree) {
      foreach ($tree as $term) {
        $termParents = $this->entityTypeManager->getStorage('taxonomy_term')->loadParents($term->id());
        if (!empty($termParents)) {
          $parentTree[] = $term->getName();
        }
      }
    }

    if (in_array($term_name, $parentTree)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Function to set a term into taxonomy vocabulary.
   *
   * @param array $line
   *   The line of csv that is been processed.
   * @param string $vocabulary_id
   *   The id of taxonomy vocabulary that terms will be set on.
   *
   * @return void
   *   This function doesn't return any value.
   */
  public function setTaxonomyTerm($line, $vocabulary_id) {
    if ($this->checkIfVocabularyExists($vocabulary_id)) {
      $previous_content = NULL;
      foreach ($line = array_diff($line, [""]) as $content) {
        // Check the first column of the line.
        if (is_null($previous_content)) {

          $term_properties = [
            'name' => $content,
            'vid' => $vocabulary_id,
          ];

          // When $previous_content is null, if term exists, it'll be a parent.
          $parent_term = $this->getTerm($term_properties);

          // Update the value of $previous_content.
          $previous_content = $content;

          // Avoid to create repeat parent term.
          if ($parent_term) {
            continue;
          }

          $this->createTerm($term_properties);
        }

        // Check the others columns.
        else {

          $termIsInTree = $this->checkTree($vocabulary_id, $content);
          // Check if term already exists in level tree.
          if ($termIsInTree) {
            $previous_content = $content;
            continue;
          }

          $term_properties = [
            'name' => $content,
            'vid' => $vocabulary_id,
          ];
          $parent_properties = [
            'name' => $previous_content,
            'vid' => $vocabulary_id,
          ];
          $parent = $this->getTerm($parent_properties);
          $parent_id = $parent->id();
          $this->createTerm($term_properties, $parent_id);
        }
      }
    }
  }

}
