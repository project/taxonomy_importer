<?php

namespace Drupal\taxonomy_importer\Batch;

use Drupal\taxonomy_importer\Helper\TaxonomiesHelper;

/**
 * Batch class.
 */
class BatchImporter {

  /**
   * Run function after batch process finish.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param array $results
   *   The value set in $context['results'] by callback_batch_operation().
   * @param mixed $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public static function finishBatch($success, $results, $operations) {
    if ($success) {
      $message = t('@count items successfully processed.', ['@count' => $results['rows_imported']]);
      \Drupal::messenger()->addStatus($message);
      return;
    }

    $message = t('An error occurred while processing');
    \Drupal::messenger()->addError($message);
  }

  /**
   * Import each line of file read.
   *
   * @param array $line
   *   The current csv line.
   * @param string $vocab_id
   *   The taxonomy vocabulary id.
   * @param array $context
   *   The batch context array.
   */
  public static function importLines(array $line, string $vocab_id, array &$context) {

    $context['results']['rows_imported']++;
    $line = array_map('base64_decode', $line);

    $message = t('Importing row @rows_imported', ['@rows_imported' => $context['results']['rows_imported']]);
    $context['message'] = $message;
    $entity_type_manager = \Drupal::entityTypeManager();

    if ($context['results']['rows_imported'] > 0) {
      // Import the current line.
      $taxonomiesHelperClass = new TaxonomiesHelper($entity_type_manager);
      $taxonomiesHelperClass->setTaxonomyTerm($line, $vocab_id);
    }
  }

}
