# Taxonomy Importer

## Installation

* By composer: composer require 'drupal/taxonomy_importer:1.0.x-dev@dev'

## Module usage

* Install the module on /admin/modules
* Go to configuration page (/admin/config/development/taxonomy-importer-csv) and fill the fields
* Go to Taxonomy page (/admin/structure/taxonomy) and check if the vocabulary and terms was created correctly.

## CSV file structure

To import the terms from CSV file correctly, use the example structure.
The first column of csv will be always be created into the taxonomy vocabulary.

Warning: We don't have limit to the hierarchical, so take care about the taxomony terms size.

